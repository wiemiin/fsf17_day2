var employee = {
    firstName: "Kenneth",
    lastName: "Phang",
    employeeNumber: 123,
    salary: 1800,
    age: 50,
    department: "Full Stack Development"
}

function getFullName() {
    console.log(employee.firstName + ' ' + employee.lastName); 
}

function isRich() {
    if (employee.salary > 2000) {
        console.log(true);
        console.log("Employee salary is more than $2000");
    } else {
        console.log(false);
        console.log("Employee salary is less than $2000");
    }
}

function belongsToDepartment() {
    if (employee.department != null) {
        console.log("Employee belongs to " + employee.department);
    } else {
        console.log("Employee does not belong to any department");
    }
}


getFullName();
isRich();
belongsToDepartment();